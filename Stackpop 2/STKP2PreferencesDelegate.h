//
//  STKP2PreferencesDelegate.h
//  Stackpop 2
//
//  Created by Jacob Budin on 2/2/14.
//  Copyright (c) 2014 Jacob Budin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import <ServiceManagement/ServiceManagement.h>
#import "GTMNSString+HTML.h"

@interface STKP2PreferencesDelegate : NSWindowController <NSWindowDelegate>
{
    NSDictionary *_user;
    NSArray *_sites;
    BOOL _authorized;
    NSString *_accessToken;
}

@property (strong) IBOutlet NSWindow *preferencesWindow;
@property (strong) IBOutlet NSWindow *authorizationWindow;
@property (weak) IBOutlet WebView *authorizationWebView;

@property (weak) IBOutlet NSProgressIndicator *progressIndicator;
@property (weak) IBOutlet NSImageView *userImageView;
@property (weak) IBOutlet NSPopUpButton *sitesPopUpButton;
@property (weak) IBOutlet NSTextField *userNameTextField;
@property (weak) IBOutlet NSButton *authorizeButton;
@property (weak) IBOutlet NSButton *reloadSitesButton;

@property NSString *accessCode;
@property NSString *accessToken;
@property (strong) NSDictionary *user;
@property (strong) NSArray *sites;
@property NSTimer *authorizationTimer;
@property BOOL authorized;

- (void)checkAuthorization:(id)sender;

@end
